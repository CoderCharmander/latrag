#include <stdio.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <getopt.h>

#define LENGTH(arr) (sizeof(arr) / sizeof(arr[0]))


enum ExMode
{
	S_AC,
	S_GN,
	S_DT,
	S_AB,
	P_NM,
	P_AC,
	P_GN,
	P_DT,
	P_AB,
};


enum Declinatio
{
	D_1,
	D_2,
	D_3,
	D_4,
	D_5,
	D_3_GYI,
	D_3_EI,
};


static char *genitivus_endings[] = { "ae", "i", "is", "us", "ei" };

static char *ragok[8][9] = {
	{"am", "ae", "ae", "a", "ae", "as", "arum", "is", "is"},
	{"um", "i", "o", "o", "i", "os", "orum", "is", "is"},
	{"em", "is", "i", "e", "es", "es", "um", "ibus", "ibus"},
	{"um", "us", "ui", "u", "us", "us", "uum", "ibus", "ibus"},
	{"em", "ei", "ei", "e", "es", "es", "erum", "ebus", "ebus"},
	{"em", "is", "i", "e", "es", "es", "ium", "ibus", "ibus"},
	{"em", "is", "i", "i", "es", "es", "ium", "ibus", "ibus"},
};

static char *ne_p_nm[7] = {
	[D_2] = "a",
	[D_3] = "a",
	[D_3_GYI] = "a",
	[D_3_EI] = "ia",
	[D_4] = "ua",
};

const char maganhangzo[] = "aeiou";

static char *nominativus;
static char *genitivus;
static int is_neutrum;

static void
read_input(void)
{
	nominativus = readline("S/NM: ");
	genitivus = readline("S/GN: ");

	printf("NE (yN)? ");
	is_neutrum = getchar() == 'y';
}


static int
nyarale(char *word)
{
	int end = strlen(word) - 2;

	return (word[end] == 'a' && (word[end + 1] == 'r' || word[end + 1] == 'l'))
	    || word[end + 1] == 'e';
}


static int
find_base(char *genitivus, int *out_base_len, int *out_declinatio)
{
	/* Get the last two characters */
	char *end_ptr = genitivus + strlen(genitivus) - 2;
	size_t i;
	for (i = 0; i < LENGTH(genitivus_endings); ++i) {
		if (0 == strcmp(end_ptr, genitivus_endings[i])) {
			*out_base_len = end_ptr - genitivus;
			*out_declinatio = (int) i;
			return 1;
		}
	}

	/* If the last char is 'i' */
	if (end_ptr[1] == 'i') {
		*out_base_len = strlen(genitivus) - 1;
		*out_declinatio = 1;    /* It begins with 0 */
		return 1;
	}

	return 0;
}

static void
append_rag(char *dest, const char *base, int declinatio, int index)
{
	if (dest != base)
		strcpy(dest, base);
	strcat(dest, ragok[declinatio][index]);
}

static int
is_mgh(char letter)
{
	return letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o'
	    || letter == 'u';
}

static int
count_syl(char *word)
{
	int count = 0;
	while (*word) {
		if (*word == 'a' && *(word + 1) == 'e') {
			++count;
			++word;
		} else if (is_mgh(*word)) {
			++count;
		}
		++word;
	}
	return count;
}

static int
is_parisyl(char *nominativus, char *genitivus)
{
	int end = strlen(nominativus) - 1;
	return count_syl(nominativus) == count_syl(genitivus)
	    && nominativus[end] == 's' && (nominativus[end - 1] == 'e' || nominativus[end - 1] == 'i'); /* -is / -es */
}


static int
is_fustli(char *nominativus, char *genitivus)
{
	const int end = strlen(nominativus) - 1;
	const int base_end = strlen(genitivus) - 2;
	return (nominativus[end] == 's' || nominativus[end] == 'x')
	    && !is_mgh(genitivus[base_end - 1])
	    && !is_mgh(genitivus[base_end - 2]);
}


int
main(int argc, char *argv[])
{
	char temp[32];
	int pos, decl;
	int is_adj = 0;
	int force_decl = -1;

	if (argc == 1) {
		read_input();
	} else {
		int opt;
		while ((opt = getopt(argc, argv, "ad:")) != -1) {
			switch (opt) {
			case 'a':
				is_adj = 1;
				break;
			case 'd':
				force_decl = atoi(optarg) - 1;
				break;
			default:
				fprintf(stderr, "Usage: %s [-a] [-d decl] <nom> <gen> m|f|n\n", argv[0]);
				return 1;
			}
		}

		if (optind > argc - 3) {
			fprintf(stderr, "Usage: %s [-a] [-d decl] <nom> <gen> m|f|n\n", argv[0]);
			return 1;
		}

		nominativus = argv[optind + 0];
		genitivus = argv[optind + 1];
		is_neutrum = 0 == strcmp(argv[optind + 2], "n");
	}

	if (!find_base(genitivus, &pos, &decl)) {
		puts("Invalid genitivus!");
		return 1;
	}

	if (force_decl != -1) {
		decl = force_decl;
		pos = strlen(genitivus) - strlen(genitivus_endings[decl]);
	}

	if (decl == 2) {
		/* Declinatio 3 is kinda weird */
		if (is_adj || nyarale(nominativus)) {
			decl = D_3_EI;
		} else if (is_parisyl(nominativus, genitivus)
		           || is_fustli(nominativus, genitivus)) {
			decl = D_3_GYI;
		}
	}
	if (is_neutrum && !ne_p_nm[decl]) {
		puts("Invalid neutrum");
		return 1;
	}

	/* Terminate at the end of the base word */
	genitivus[pos] = '\0';
	strcpy(temp, genitivus);
	/* Nominativus first */
	puts(nominativus);
	/* Iterate now */
	for (int i = 0; i < 9; ++i) {
		if (is_neutrum) {
			if (decl == D_4 && is_neutrum && i < P_NM && i != S_GN) {
				strcpy(temp, genitivus);
				strcat(temp, "u");
				puts(temp);
				continue;
			}
			switch (i) {
			case S_AC:
				puts(nominativus);
				continue;
			case P_NM:         /* FALLTHROUGH */
			case P_AC:
				strcpy(temp, genitivus);
				strcat(temp, ne_p_nm[decl]);
				puts(temp);
				continue;
			}
		}

		append_rag(temp, genitivus, decl, i);
		puts(temp);
	}
}
