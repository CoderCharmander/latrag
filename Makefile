PRODUCT := latrag
VERSION := 1.0
EXPORT = $(PRODUCT)-$(VERSION)

CFLAGS ?= -Wall -Wextra -g
LD := gcc
LDFLAGS += -lreadline -g

OBJECTS := latrag.o

.PHONY: all clean dist

all: $(PRODUCT)

clean:
	rm -f $(PRODUCT) $(OBJECTS)

dist: clean
	mkdir -p $(EXPORT)
	cp -f \
		$(subst .o,.c,$(OBJECTS)) \
		$(wildcard *.h) \
		Makefile \
		$(EXPORT)
	tar cJf $(EXPORT).tar.xz $(EXPORT)
	rm -rf $(EXPORT)
